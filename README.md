NB We have developed a more sophisticated model for estimated biological effects on snow and ice albedo, available at: https://doi.org/10.5281/zenodo.1048811. We recommend using the new model. 

## README ##

This README gives general usage information for bioalbedo.py, which is a model used to predict the spectral albedo of a snowpack from user defined snow physics, algal pigmentation and concentration, plus solar irradiance data.

### What is this repository for? ###

* The repository includes the bioalbedo model and all dependencies. To run the model, the user must run Python 3.4, download the scripts and data here into their working directory and also download the radiative transfer scheme TARTES (most easily available using pip: pip install tartes).

* This version of the model is an initial release that requires validation against field data. A more intuitive user-interface is desirable to facilitate wider usage.


### MODEL ###

The model is primarily controlled using BIOALBEDO.py
Running this script with all defaults will output two figures. One shows spectral albedo for a snowpack with seven orders of magnitude of algal biomass along with an algae free snowpack.

Soot concentration in the algae-free snowpack is defined in line 70. Soot concentration in the algal snowpack is defined in line 71.
Values for algal biomass can be defined in line 98.

Snowpack characteristics are determined in the script snow.py. In lines 112 to 116 the properties of each layer are defined. More layers can be added as many times as necessary by copying and pasting the bottom line.

Optical properties of algal cells are defined in the script cells.py. In lines 56 to 63 the mass fractions of each pigment are defined.


### TO SET-UP AND RUN ###

In the working directory, tartes-0.9.3, BIOALBEDO.py, cells.py, bioutils.py, models.py, albedo_reporting.py, snow.py and the dataset solar.csv should exist. 
The model is run simply by running BIOALBEDO.py. Variables are user-defined as explained above.
The modules sys, numpy, scipy, pandas, matplotlib.pyplot are required.

To model albedo at different locations or times, new incoming solar radiation data must be measured or modelled and saved as a csv in the working directory. It must either be saved as 'solar.csv' or the filename must be updated in the read_csv command in BIOALBEDO.py. We used NASA's COART model (cloudsgate2.larc.nasa.gov/jin/coart.html). Another option is SBDART (http://ncgia.ucsb.edu/projects/metadata/standard/uses/sbdart.htm) which can be run online or the FORTRAN source code can be downloaded.


### CONTRIBUTION GUIDELINES ###

The most desirable contributions are currently a more user-friendly interface that would enable non-coders to use the model and the integration of a Lorenz-Mie scheme for characterizing scattering effects. We would also like to exploit TARTES' irradiance profiles, actinic flux and absorption profile capabilities to characterize the impact of algal blooms on the subsurface light field.

### MELT ###

The broadband albedo values can be used to drive a snow energy balance model. We have used Brock and Arnold (2000)'s point-surface energy balance model for this purpose. This is an excel model that also requires site and meteorological data. Brock and Arnold's (2000) EB-Model is available from the publishers website here: http://onlinelibrary.wiley.com/doi/10.1002/1096-9837(200006)25:6%3C649::AID-ESP97%3E3.0.CO;2-U/abstract


### CONTACT ###

These codes were written by Joseph Cook and Angus Taggart (University of Sheffield).
For correspondence: joe.cook@sheffield.ac.uk

## USAGE ##

We encourage other researchers to use and/or improve our model. We would also be pleased to collaborate. We would appreciate citation of our paper 'A physical model for the 'bioalbedo' of snow' (full reference to be added after final publication) if the model is used in published work.

## DISCLAIMER ##

We provide free and open access to this software on the understanding that it requires validation and further rigorous testing - the authors assume no responsibility for downstream usage and, while we welcome collaboration, we are not obliged to provide training or support for users. 
We hope that this code and data will be useful and encourage collaboration, but we provide it WITHOUT WARRANTY or even the implied warranty of merchantability or fitness for any particular purpose.
