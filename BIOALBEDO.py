#-*- coding: utf-8 -*-
"""
Created on Fri June 24 17:31:19 2016
@author: Joseph
"""
# Code to predict spectral and broadband bioalbedo of a snowpack.
# Dependencies:
#   the following must be in the working directory:
#     CODE: cells.py, snow.py, models.py, albedo_reporting.py, tartes-0.9.3 (or higher)
#     DATA: solar.csv (incoming solar radiation, measured or modelled e.g. by SBDART or COART)
#   PYTHON MODULES: sys, numpy, pyplot, pandas, scipy 

import sys
import tartes
import numpy as np
from tartes.impurities import Soot, HULIS
import bioutils
import logging
import cells
import albedo_reporting as ar
import snow as snowpack
import models
import matplotlib.pyplot as plt
import scipy as sci
import pandas as pd


sys.path.append('D:\\Documents\\rtm_code\\tartes-0.9.3') # amend to user's own working directory
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

# Initiate lists to append data to later
BBlist=[]
bioconc_list = []

#import incoming irradiance (modelled using http://cloudsgate2.larc.nasa.gov/jin/coart.html 
# and saved in working directory as 'solar.csv' for convolving with reflectance later. 
# Replace and NaNs with 0, set minimum value to 0.0000001 to avoid div/zero error.
incoming = pd.read_csv('solar_lutz.csv')
incoming.columns = ['wl','dif_down','dir_down','tot_down','tot_up']
incoming.fillna(0)
incoming = np.array(incoming['tot_down'])

for i in np.arange(len(incoming)):
    if incoming[i] == 0.0:
        incoming[i] = 0.00001

# Initiate figure
plt.figure(figsize=(7,6))

# Create wavelength ranges for visible (bio) and full solar spectra
min_wl = 400
max_wl = 2501

min_bio_wl = 400
max_bio_wl = 750

wavelengths = bioutils.wavelength_array_m(min_wl, max_wl)
wavelengths_bio = bioutils.wavelength_array_m(min_bio_wl,max_bio_wl)


# Define impurities.
# For biological impurities generate species by calling 'generate_species' function in cells.py
# To define the pigment concentration in the cells, modify in cells.py
# For soot define quantity of soot in top layer. Change layer id to add to subsurface layer.
# Layer id's are 'top' for surface, then numerically 1-x for subsurface layers.

# For soot, 'topsoot' defines concentration in clean snowpack, topsoot_bio is for the algal snowpack.
bio1 = cells.generate_species('chlamydomonas',400,750)
topsoot = 1e-9
topsoot_bio = 1e-9



# create clean snowpack by calling snowpack function in snow.py and add soot. To define
# snow characteristics modify 'default_snowpack' in snow.py
clean = snowpack.default_snowpack()
clean.add_impurity(layer_id='top',impurity=Soot,mass=topsoot)

# calculate reflectance for the clean snowpack, then conbvolve with incoming 
# solar radiation to give albedo. np.errstate method avoids errors from divide by zero
reflect_clean = snowpack.snowpack_albedo(wavelengths,clean)
with np.errstate(divide='ignore',invalid='ignore'):
        temp_clean = reflect_clean[0:1801] * incoming
        albedo_clean = temp_clean/incoming
        BB_clean = sci.integrate.simps(temp_clean) / sci.integrate.simps(incoming)
        BB_clean_vis = sci.integrate.simps(temp_clean[0:301]) / sci.integrate.simps(incoming[0:301])        








# Establish loop to interate through a range of biomass concentrations in algal snowpack and 
# calculate algal snow albedo.
for bioconc in (1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3):
    # Two snowpacks are created so that one can be visible-only and incorporate algae, one is
    # full waveband and algae-free. These merge later in the code.
    dirty = snowpack.default_snowpack()
    dirty_bio = snowpack.default_snowpack()
    
    # Add impurities
    dirty.add_impurity(layer_id='top',impurity=Soot,mass=topsoot_bio)
    dirty_bio.add_impurity(layer_id='top',impurity=Soot,mass=topsoot_bio)
    dirty_bio.add_impurity(layer_id='top',impurity=bio1,mass=bioconc)

    # calculate reflectance for both snowpacks and combine
    reflect_dirty = snowpack.snowpack_albedo(wavelengths,dirty)
    reflect_dirty_bio = snowpack.snowpack_albedo(wavelengths_bio,dirty_bio)
    reflect_dirty[0:351] = reflect_dirty_bio[0:351]

       
    # convolve with incoming solar radiation to give albedo. The division is
    # completed using 'with np.errstate' code to avoid errors from dividing by zero
    with np.errstate(divide='ignore',invalid='ignore'):
        temp = reflect_dirty[0:1801] * incoming
        albedo = temp/incoming        

    # integrate over waveband to provide broadband albedo 
    albedo_int = sci.integrate.simps(temp) / sci.integrate.simps(incoming)
    albedo_int_vis = sci.integrate.simps(temp[0:301]) / sci.integrate.simps(incoming[0:301])

    # define values as strings for labelling plots 
    label1 = str(bioconc)
    label2 = str(np.around(albedo_int,2))
    label3 = str(np.around(albedo_int_vis,3))

    # Append biomass and broadband albedo to lists for plotting
    bioconc_list.append(bioconc)
    BBlist.append(albedo_int)
    
    # plot wavelengths against albedo for each biomass.
    plt.plot(wavelengths[0:1801]*1e9,albedo,label=('{}, broadband = {})'.format(label1,label2)))







# tidy up plot outside of loop, and add clean snow albedo for comparison
label4 = str(np.around(BB_clean,2))
plt.plot(wavelengths[0:1801]*1e9,albedo_clean,'g',label='Clean Snow, BB = {}'.format(label4))
plt.ylim(0,1)
plt.xlabel('Wavelength')
plt.ylabel('Reflectance')
plt.legend(bbox_to_anchor=(1.92,1),frameon=False,ncol=2)
plt.legend(loc='best')


# Plot broadband albedo vs biomass concentration from lists
plt.figure(figsize=(7,6))
plt.plot(bioconc_list,BBlist)
plt.xlabel('Biomass g snow/g algae')
plt.ylabel('Broadband albedo (400-2200 nm)')
plt.savefig('BB vs biomass_light_pigments.tif',dpi=300)



