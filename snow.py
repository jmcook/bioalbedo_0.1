import tartes

def snowpack_albedo(wlnano, snowpack):
    print('----------------------------------------------------------------------------------------------------------')
    print(snowpack.impurities, snowpack.layers)
    print('----------------------------------------------------------------------------------------------------------')
    if len(snowpack.impurities) == 0:
        return tartes.albedo(wlnano,
                             SSA=snowpack.ssa,
                             density=snowpack.density,
                             thickness=snowpack.thickness
                             )
    else:
        return tartes.albedo(wlnano,
                             SSA=snowpack.ssa,
                             density=snowpack.density,
                             thickness=snowpack.thickness,
                             impurities_type=snowpack.impurities,
                             impurities=snowpack.layers
                             )


class snowpack:
    def __init__(self, ssa=None, density=None, thickness=None):
        if ssa is None:
            self.ssa = []
        else:
            self.ssa = ssa

        if density is None:
            self.density = []
        else:
            self.density = density

        if thickness is None:
            self.thickness = []
        else:
            self.thickness = thickness

        self.impurities = []
        self.layers = []

    #
    # def __getitem__(self, item):
    #     return dict(zip(self.impurities_type, self.impurities[item]))
    #
    # def __setitem__(self, key, value):
    #     if len(self.ssa) < key:
    #         raise IndexError



    def __getattr__(self, attr):
        if attr == 'layers':
            return len(self.ssa)


    def get_layer_id(self, layername):
        if layername == 'bottom':
            return len(self.layers)
        elif layername == 'top':
            return 0
        elif isinstance(layername, int):
            return layername
        else:
            raise

    def add_layer(self, ssa, density, thickness, layer_position='bottom'):

        layerid = self.get_layer_id(layer_position)
        if layerid == -1:
            layerid = len(self.layers)
        self.ssa.insert(layerid, ssa)
        self.density.insert(layerid, density)
        self.thickness.insert(layerid, thickness)
        self.layers.insert(layerid, [0] * len(self.impurities)) # add layers empty of impurities



    def add_impurity(self, layer_id, impurity, mass):
        try:
            impurity_id = self.impurities.index(impurity)
        except ValueError: # impurity not in list
            self.impurities.append(impurity)
            impurity_id = self.impurities.index(impurity)
            for id in range(len(self.layers)):
                self.layers[id].append(0)

        # append impurities to each of the layers

        if isinstance(layer_id, list) or isinstance(mass, list):
            if len(layer_id) != len(mass):
                raise IndexError('layer length and mass length should be the same')
            else:
                for n, i in enumerate(layer_id):
                    self.layers[self.get_layer_id(i)][impurity_id] = mass[n]


        else: #layer and mass are not lists assuming they are ints
            self.layers[self.get_layer_id(layer_id)][impurity_id] = mass

    def get_impurities(self):
        return self.impurities, self.layers



# Manually alter SSA, density or layer thicknesses here to change snow physics in BIOALBEDO.py.
# Default is for melting snowpack.

def default_snowpack():
    sp = snowpack()
    sp.add_layer(ssa=1, density=500, thickness=0.001, layer_position='top')
    sp.add_layer(ssa=1, density=500, thickness=0.1, layer_position='bottom')
    sp.add_layer(ssa=1, density=500, thickness=1, layer_position='bottom')
    sp.add_layer(ssa=1,density=500,thickness=1,layer_position='bottom')
    sp.add_layer(ssa=1,density=500,thickness=1,layer_position='bottom')    
    return sp
