# -*- coding: utf-8 -*-
"""
Created on Mon Nov 27 13:36:28 2017

@author: joe
"""
###### This code calculates the mie optical parameters for an algal cell ##### 

# Code takes empirically derived values for bio-optical parameters for algal cells
# and use them to determine the wavelength-dependent imaginary part (k) of the
# complex index of refraction n' = n + ik

# This is then fed into Lorenz-Mie equations to derive absorption cross 
# section, qqabs.

# P = density of dry material (kg m^-3)
# Ea(i,wl) = in vivo absorption coefficient for each pigment per wavelength (m^2 kg^-1)
# Wi = dry mass fraction of pigment in cell (kg kg^-1 biomass)
# Xw = water fraction in each cell (dimensionless)
# Np = cell numer density (m^-3)
# V32 = mean sauter diameter (m^3)
# Cx = biomass dry weight concentration (g/L which is equal to kg/m3)
# wlmin = minimum wavelength in measured spectrum (nm)
# wlmax = maximum wavelength in measured spectrum (nm)
# dw = spectral resolution (e.g. 1nm) 

# the package 'miepython' needs to be installed and added to the python environment
# miepython is available on pip. 

# pip install miepython

import numpy as np
import matplotlib.pyplot as plt
import scipy as sci
from scipy.integrate import quad
import csv
from math import exp
from miepython import mie

# set up empty lists to append to when iteratively calculating parameter values
# later.

WL = []
WLnano=[]
Ea1 = []
Ea2 = []
Ea3 = []
Ea4 = []
EW1 = []
EW2 = []
EW3 = []
EW4 = []
K=[]
klist = []
Qlist = []

# Read in wavelength dependent in vivo absorption coefficients for each pigment
# from excel documents in  working directory

with open('wavelengths.csv')as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        for cell in row:
            cellf = float(cell)
            cellf_m = cellf*10E-10
            WL.append(cellf_m)

## expand WL array down to 300nm so that arrays are all same length (<350 discarded later)
 
xx = [i*10E-10 for i in range(300,350)] 
xx.extend(WL)
WL = xx


# Also create new array containing wavelengths in nanometers rather than meters
# (for integration and plotting)

WLnano = [a*1e9 for a in (WL)]
WLnano2 = []
for a in WLnano:
    WLnano2.append(np.int(a))


with open('chlorophyll-a.csv')as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        for cell in row:
            cellf = float(cell)
            Ea1.append(cellf)


with open('chlorophyll-b.csv') as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        for cell in row:
            cellf = float(cell)
            Ea2.append(cellf)

with open('Photoprotective_carotenoids.csv')as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        for cell in row:
            cellf = float(cell)            
            Ea3.append(cellf)
        
    
with open('Photosynthetic_carotenoids.csv')as f:
    reader = csv.reader(f,delimiter=',')
    for row in reader:
        for cell in row:
            cellf = float(cell)
            Ea4.append(cellf)

# extend spectral data down to 300nm by padding with the value at 350nm

Ea1 = [Ea1[0] for _ in range(50)] + Ea1
Ea2 = [Ea2[0] for _ in range(50)] + Ea2
Ea3 = [Ea3[0] for _ in range(50)] + Ea3
Ea4 = [Ea4[0] for _ in range(50)] + Ea4

# for full model equations see Pottier et al (2005). 
# Assign mass fraction of each pigment in cell. W1 = Chll a, W2 = chll b,
# W3 = photoprotective carotenoids, W4 = photosynthetic carotenoids

W1 = 0.0193
W2 = 0.01
W3 = 0.1
W4 = 0.01

# Multiply mass fraction of each pigment by absorption coefficient at each wavelenth

EW1 = [a * W1 for a in Ea1]
EW2 = [a * W2 for a in Ea2]
EW3 = [a * W3 for a in Ea3]
EW4 = [a * W4 for a in Ea4]

# Sum all pigments
EWW = [sum(x) for x in zip(EW1,EW2,EW3,EW4)]

# Assign values for X, Xw, Rho and nm.
# X is the size parameter calculated in a separate script
# Xw is water fraction in cell. Dauchet et al (2015) shows it can be 
# assumed constant at 0.8. 
# Rho is density of dry material. Dauchet et al (2015) show this can be assumed
# constant at 1400 for algal cells
# nm is the real part of the refractive index of the surrounding medium, i.e. ice.

Xw = 0.8 # water fraction in cell (can be assumed constant 0.8)
Rho = 1400 # density of dry material (can be assumed constant 1400)
nm = 1.5 # real part of RI

# Calculate refractive index, K
for i in WL:
    k = (i/(4*np.pi)) * Rho * ((1-Xw)/Xw) 
    klist.append(k)
    
K = [a*b for a,b in zip(EWW,klist)]
K = np.array(K) # numpy array containign spectral imaginary part of RI

# Define variable values to feed mie solver
WL = np.array(WL)*1e6
radius = 10                    # in microns
num = len(WL)
m = nm-1.0j*K
X = 2*np.pi*radius/WL
density = 1400 # cell density in kg m-3

# set up empty arrays for optical params
qqabs = np.zeros(num) # absorption cross section
qqsca = np.zeros(num) # scattering cross section
qqext = np.zeros(num) # extinction cross section
MAC = np.zeros(num) # mass-specific absorption

# run mie solver per unit wavelength
for i in range(num) :
    qext, qsca, qback, g = mie(m[i],X[i]) # call mie solver
    qabs = qext - qsca # calculate absorption efficiency
    qqabs[i]=qabs*np.pi*radius**2 # calculate cross section from efficiency
    qqsca[i]=qsca*np.pi*radius**2 # calculate cross section from efficiency
    qqext[i]=qext*np.pi*radius**2 # calculate cross section from efficiency
    MAC[i] = qabs/((4/3*np.pi*(radius*1e6)**2)*density) # calculate MAC



if __name__ == '__main__':
    plt.figure(1)
    plt.plot(WLnano,Ea1,label='Chlorophyll a')
    plt.plot(WLnano,Ea2,label='Chlorophyll b')
    plt.plot(WLnano,Ea3,label='Secpndary carotenoids')
    plt.plot(WLnano,Ea4,label='Primary carotenoids')
    plt.xlabel('Wavelengths nm')
    plt.ylabel('In vivo absorption coefficient')
    plt.xlim(350,750)
    plt.legend(loc='best')
    plt.show()

    plt.figure(2)
    plt.plot(WL,qqabs,'b',label='absorption cross section')
    plt.plot(WL,qqsca,'g',label='scattering cross section')
    plt.plot(WL,qqext,'r', label= 'extinction cross section')
    plt.ylabel(r"Cross Section ($\mu$m$^2$)")
    plt.xlabel('Wavelength (nm)')
    
    plt.figure(3)
    plt.plot(WL,MAC)
    plt.xlabel('Wavelength (nm)')
    plt.ylabel('MAC algae (m$^2$ / kg)')
    plt.legend(loc='best')