import scipy as sci
import datetime
import matplotlib.pyplot as plt
import os.path
import cells

report_id = 0
report_figure_id = 0



def get_report_file(report_filename = None, now=None):
    '''
    open a reporting file and return the handle
    :param now:
    :return:
    '''
    global report_id, report_figure_id

    report_figure_id = 0

    if report_filename is None:
        got_report_id = False
        while not got_report_id:
            report_filename = 'Bio_RTM_{}.txt'.format(report_id)
            if not os.path.isfile(report_filename):
                fh = open(report_filename, 'w')
                got_report_id = True
            else:
                try:
                    report_id += 1
                except:
                    report_id = 0
    else:
        report_id = report_filename
        report_filename = 'Bio_RTM_{}.txt'.format(report_id)
        fh = open(report_filename, 'w')

    if now is None:
        now = datetime.datetime.now()

    report_time = now.strftime('%Y%m%d%H%M%S')

    fh.write('\n\n-------------Report {} @ {}----------------\n'.format(report_id, now.strftime('%Y %m %d - %H:%M:%S')))

    return now, fh


def report_snowpack_and_cells(snowpack, description=''):
    global report_id, report_figure_id
    report = '\n---- {} snowpack makeup ----\n'.format(description)
    report += '{} layer snowpack\n'.format(len(snowpack.layers))

    report += 'SSA: {}\n'.format(snowpack.ssa)
    report += 'density: {} \n'.format(snowpack.density)
    report += 'thickness: {}m \n'.format(snowpack.thickness)

    report += '\nimpurities:\n'.format()
    for n,i in enumerate(snowpack.impurities):

        report += '{}: \n'.format(i)

        if isinstance(i, cells.PigmentedCell):
            for dye, values in i.pigments.items():
                report += '\t\t{}: {}g/g\n'.format(dye, values['mass_fraction'])
            report += '\n'

            plt.figure()
            plt.title('Absorption spectrum for each pigment')
            for name, pigment in i.pigments.items():
                plt.plot(range(350, 350+len(pigment['abs'])), pigment['abs'], label=name)
            plt.xlabel('Wavelengths nm')
            plt.ylabel('Absorption coefficient')
            plt.xlim(350, 750)
            plt.legend(bbox_to_anchor=(1, 1), loc='upper left', ncol=1)
            plt.savefig('fig_{}-{}.png'.format(report_id, report_figure_id))
            report += 'see figure: fig_{}-{}.png for cells absorbtion by dye\n'.format(report_id, report_figure_id)
            report_figure_id += 1

            plt.figure()
            plt.title('cell absorption')
            plt.plot(range(350, 350+len(i.K)), i.K)
            plt.xlabel('Wavelength (nm)')
            plt.xlim(350, 750)
            plt.ylabel('k')
            plt.savefig('fig_{}-{}.png'.format(report_id, report_figure_id))
            report += 'see figure: fig_{}-{}.png for cells absorbtion \n'.format(report_id, report_figure_id)
            report_figure_id += 1

        for m, l in enumerate(snowpack.layers):
            report += '\t\tlayer {}: {}\n'.format(m, l[n])

    # print relevant values

    return report


def report_albedo(albedo, wavelengths, append=False, label=""):
    global report_id, report_figure_id
    Albedo_int = sci.integrate.simps(albedo) / len(wavelengths)
    # print relevant values
    if not append:
        plt.figure(1)
    plt.subplot(211)
    plt.title('Spectrally-integrated reflectance of snow with algae = {}'.format(Albedo_int))
    plt.plot(wavelengths * 1e9, albedo, label='Algal snow' + label)
    plt.xlabel('Wavelength')
    plt.ylabel('Albedo')
    plt.xlim(400, 700)
    plt.legend(loc='best')
    
    if not append:    
        plt.savefig('fig_{}-{}.png'.format(report_id, report_figure_id))

    report = 'see figure: fig_{}-{}.png\n'.format(report_id, report_figure_id)
    report_figure_id += 1
    return report


def report_albedo_impact(albedo1, reference_snow, wavelengths):
    global report_id, report_figure_id
    report = ''
    plt.subplot(211)
    plt.plot(wavelengths * 1e9, reference_snow, label='Pure Snow')
    plt.legend(loc='best')

    # Calculate change in albedo relative to pure snow and average over visible
    # waveband

    deltaAalg = [a - b for a, b in zip(reference_snow, albedo1)]
    deltaAalgav = sum(deltaAalg) / len(deltaAalg)

    # Calculate spectrally-integrated reflectance over visible waveband (using
    # Simpson's method) for pure and algal snow

    Albedo_pure_int = sci.integrate.simps(reference_snow) / len(wavelengths)

    # Plot change in albedo on separate figure
    #plt.figure(2)

    plt.subplot(212)
    plt.plot(wavelengths * 1E9, deltaAalg, label='Reflectance change compared to pure snow')
    plt.xlabel('wavelength')
    plt.ylabel('Change in Albedo')
    plt.xlim(400, 700)
    plt.legend(loc='best')

    plt.savefig('fig_{}-{}.png'.format(report_id, report_figure_id))
    report_figure_id += 1

    report += '\n---- Albedo -----\n'
    report += 'Spectrally-integrated reflectance of pure snow: {}\n'.format(Albedo_pure_int)

    Albedo_int = sci.integrate.simps(albedo1) / len(wavelengths)
    report += 'Spectrally-integrated reflectance of impure snow: {}\n'.format(Albedo_int)

    report += 'Mean reflectance change over vis waveband compared to pure snow: {}\n'.format(deltaAalgav)
    report += 'see figure: fig_{}-{}.png\n\n'.format(report_id, report_figure_id)


    return report
